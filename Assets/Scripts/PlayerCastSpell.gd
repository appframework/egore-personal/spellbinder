extends "./BaseCastSpell.gd"

var spells
var raycast
var spellIndex

signal spell_changed(spell)

func init():
	spells = get_node("/root/Level/Avatar/player").profession.availableSpells
	select_spell(0)
	raycast = $"/root/Level/Avatar/Main Camera3D/RayCast3D"

func select_spell(index: int) -> void:
	spellIndex = index
	var spell_ = get_node(spells[index])
	self.spell = spell_
	emit_signal("spell_changed", spell_)

func _input(event):
	if event is InputEventKey:
		if Input.is_action_just_pressed("spell_1") && spells.size() > 0:
			select_spell(0)
		elif Input.is_action_just_pressed("spell_2") && spells.size() > 1:
			select_spell(1)
		elif Input.is_action_just_pressed("spell_3") && spells.size() > 2:
			select_spell(2)
		elif Input.is_action_just_pressed("spell_4") && spells.size() > 3:
			select_spell(3)
		elif Input.is_action_just_pressed("spell_5") && spells.size() > 4:
			select_spell(4)
	elif event is InputEventMouseButton:
		if Input.is_action_just_pressed("fire"):
			castSpell(raycast)
		elif event.is_pressed() && event.button_index == MOUSE_BUTTON_WHEEL_UP:
			spellIndex += 1
			if (spellIndex >= spells.size()):
				spellIndex = 0
			select_spell(spellIndex)
		elif event.is_pressed() && event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
			spellIndex -= 1
			if (spellIndex < 0):
				spellIndex = spells.size() - 1
			select_spell(spellIndex)

