extends CharacterBody3D
class_name FirstPersonController

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

# How fast a mouse movement will rotate/pitch the camera
@export var mouseSensitivity: float = 0.07
# How far up and down (in degrees) the player can look
@export var mouseRange: float = 60.0
# Speed the character is able to move
@export var movementSpeed: float = 10.0
# Jump speed the character has
@export var jumpVelocity: float = 4.5
#
const SPEED := 5.0

@export var team: Team
@export var profession: Profession

# The current vector as passed to move_and_slide
var velocity_ = Vector3()

var anim: AnimationPlayer
var camera: Camera3D
var bias: Node
var health: Node

# Speed the character has vertically (e.g. gravity or jumping)
var verticalVelocity = 0.0

# Obtain a reference to the character controller
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$player.playername = 'You!'
	$player.profession = profession
	$player.team = team
	anim = $player/AnimationPlayer
	camera = $"Main Camera3D"
	bias = $Bias
	health = $Health
	$PlayerCastSpell.init()

func _input(event):
	if event is InputEventMouseMotion:
		# Rotation
		rotate_y(deg_to_rad(-event.relative.x * mouseSensitivity)) 
		camera.rotate_x(deg_to_rad(-event.relative.y * mouseSensitivity)) 
		camera.rotation.x = clamp(camera.rotation.x, deg_to_rad(-mouseRange), deg_to_rad(mouseRange))

func _physics_process(delta):

	if Input.is_action_just_pressed("quit"):
		get_tree().quit()

	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = jumpVelocity

	# Get the input direction and handle the movement/deceleration.
	var input_dir = Input.get_vector("left", "right", "forward", "backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()

	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
		anim.current_animation = "Running"
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
		anim.current_animation = "Standing"

	move_and_slide()
