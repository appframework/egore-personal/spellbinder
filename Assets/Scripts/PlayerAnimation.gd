using UnityEngine;
using System;

public class PlayerAnimation : MonoBehaviour
{
	public bool Running;
	Animator anim;

	void Awake()
	{
		anim = GetComponentInChildren<Animator>();
	}

	void Update()
	{
		anim.SetBool("IsRunning", Running);
	}
}
