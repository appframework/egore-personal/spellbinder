extends Node
class_name Health

@export var currentHealth: int
@export var maxHealth: int = 100
var effects = []

signal health_changed(value)

func _ready():
	currentHealth = maxHealth

func _process(delta):
	var effectsToRemove = []
	for effect in effects:
		if delta - effect.timestamp >= effect.effect.duration:
			effectsToRemove.Add(effect)
		if delta - effect.lastApplied >= 1.0:
			if effect.effect.damagePerSecond != 0:
				doDamage(effect.effect.damagePerSecond)
			effect.lastApplied = delta
	for effect in effectsToRemove:
		effects.Remove(effect)

func doDamage(amount: int) -> void:
	currentHealth -= amount
	currentHealth = min(currentHealth, maxHealth)
	emit_signal("health_changed", currentHealth)
	if currentHealth <= 0:
		# Respawn
		#TODO gameObject.transform.position = Bots.GetSpawnLocation(GetComponent<Player>().team)
		currentHealth = maxHealth
		effects.clear()

func applyEffect(effect: Effect) -> void:
	# If the effect already existed, remove_at it
	for existingEffect in effects:
		if existingEffect.effect == effect:
			effects.Remove(existingEffect)
			break
	# Apply the new effect
	var appliedEffect = AppliedEffect.new()
	#appliedEffect.timestamp = 
	appliedEffect.effect = effect
	effects.push_back(appliedEffect)
