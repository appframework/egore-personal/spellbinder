using UnityEngine;

public class Waypoint : MonoBehaviour
{

	void OnDrawGizmos()
	{
		Gizmos.color = Color.CYAN;
		Gizmos.DrawCube(transform.position + (Vector3.up * 0.5f), new Vector3(0.1f, 1.0f, 0.1f));
		Gizmos.DrawSphere(transform.position + Vector3.up, 0.25f);
	}
}
