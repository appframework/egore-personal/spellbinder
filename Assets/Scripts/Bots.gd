extends Node
class_name Bots

@export var bot: PackedScene
@export var teams: Array[NodePath]
@export var professions: Array[NodePath]
@export var maxPlayers: int = 0 # FIXME 24
@export var cooldown: float = 1.0

var botNumber = 1
var cooldownRemaining = cooldown
var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()

func getNexus(team: Team) -> Nexus:
	var nexi = get_tree().get_nodes_in_group("Nexus")
	for nexus in nexi:
		if nexus.owningTeam == team && nexus.currentTeam == team:
			return nexus
	return null

func createBot() -> void:
	# Create the bot an add it to the scene, so all _ready() methods get called
	var botInstance = bot.instantiate()
	self.add_child(botInstance)

	# Set the name, team, profession and spell
	botInstance.player.playername = "Bot %d" % botNumber
	botNumber += 1
	botInstance.player.profession = get_node(professions[rng.randi_range(0, professions.size() - 1)])
	botInstance.player.team = get_node(teams[rng.randi_range(0, teams.size() - 1)])
	botInstance.aiCastSpell.spell = botInstance.player.profession.availableSpells[rng.randi_range(0, botInstance.player.profession.availableSpells.size() - 1)]

	# Create a bot looking into a random direction
	var nexus = getNexus(botInstance.player.team)
	botInstance.global_transform = nexus.global_transform

	botInstance.label.text = botInstance.name
	botInstance.label.set("custom_colors/font_color", botInstance.player.team.color)
	botInstance.bias.nexus = nexus

	# Assign it the material matching its team
	var player = botInstance.player.get_node('Armature/Skeleton3D/Player')

	# TODO: the material is global, hence the last bot will set all bots color
	var x: ArrayMesh = player.mesh
	var y: StandardMaterial3D = x.surface_get_material(2)
	y = y.duplicate()
	y.albedo_color = botInstance.player.team.color
	x.surface_set_material(2, y)

func _process(delta):
	cooldownRemaining -= delta
	if cooldownRemaining <= 0.0:
		var activePlayers = 0
		for team in teams:
			activePlayers += get_node(team).players.size()
		if activePlayers < maxPlayers:
			createBot()
		cooldownRemaining = cooldown
