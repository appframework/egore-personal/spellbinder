# Base functionality for all spell casts
extends Node
   
# Currently active cooldown
@export var cooldownRemaining: float
# The spell to cast
var spell: Spell

func _process(delta):
	cooldownRemaining -= delta

func castSpell(raycast):
	if !spell:
		return

	if cooldownRemaining <= 0.0:
		cooldownRemaining = spell.cooldown
		spell.cast(self, raycast)
		if (spell.healthCost != 0):
			get_parent().get_node("Health").doDamage(spell.healthCost)
