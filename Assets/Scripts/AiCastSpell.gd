extends 'BaseCastSpell.gd'

# Spell cast from our AI. It will try to fire whenever the target has a health
# (so right now it might attack team members).

var ray

func _ready():
	ray = get_parent().get_node("RayCast3D")

func _process(_delta):
	# We are rotated towards our target, so check if there is someone in our crosshair
	if ray.is_colliding():
		var opponent = ray.get_collider()
		if opponent.is_in_group("Player"):
			castSpell(ray)
