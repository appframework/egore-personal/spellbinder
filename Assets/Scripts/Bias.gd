# The player is biasing a nexus / earthnode.
extends Node
class_name Bias

@export var amount: float = 5.0
@export var infoText: Label
var nexusImage

var nexus: Nexus : get = nexus_get, set = nexus_set

var biasParticles: GPUParticles3D

func _ready():
	biasParticles = get_node("/root/Level/Avatar/GPUParticles3D")
	infoText = get_node("/root/Level/UI/Infotext")
	nexusImage = get_node("/root/Level/UI/Nexus status")

func _process(delta):
	if nexus:
		# When the player is within range of a nexus and wants to bias it, do it
		if Input.is_action_pressed("bias"):
			var playerTeam = get_node("/root/Level/Avatar/player").team
			if !biasParticles.visible:
				biasParticles.process_material.color = playerTeam.color
				biasParticles.visible = true
			nexus.bias(delta * amount, playerTeam)
		else:
			biasParticles.visible = false

		infoText.text = "In Nexus %s" % nexus.owningTeam.name
		if nexus.currentTeam:
			#TODO do using signal
			nexusImage.visible = true
			nexusImage.texture_progress = nexus.currentTeam.nexusImage
			nexusImage.min_value = 0
			nexusImage.max_value = nexus.maxHealth
		else:
			nexusImage.visible = false
		nexusImage.value = nexus.health
	else:
		infoText.text = ""
		biasParticles.visible = false

func nexus_get() -> Nexus:
	return nexus

func nexus_set(other: Nexus) -> void:
	nexus = other
	biasParticles.visible = !!other
	nexusImage.visible = !!other
