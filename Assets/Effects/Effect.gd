# An "effect" is the result of a spell.
extends Node
class_name Effect

@export var effectUiImage: CompressedTexture2D
@export var damagePerSecond: int
@export var duration: int
