# An applied effect is a specific effect (e.g. bleeding) applied to a player
extends Resource
class_name AppliedEffect

var timestamp: float
var lastApplied: float
var effect: Effect
