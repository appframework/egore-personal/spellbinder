extends Label

func _ready():
	var baseCastSpell = get_node("/root/Level/Avatar/PlayerCastSpell")
	baseCastSpell.connect("spell_changed", spell_changed)

func spell_changed(spell):
	if spell:
		text = spell.name
	else:
		text = "No spell selected"
