extends Label

@export var matchDuration: int = 60 * 60

var begin: int = 0

func _ready():
	begin = Time.get_unix_time_from_system() as int

func _process(_delta):
	var secondsRemaining: int = matchDuration - (Time.get_unix_time_from_system() - begin)
	if secondsRemaining < 0:
		get_tree().quit()
	self.text = "%02d:%02d" % [secondsRemaining / 60, secondsRemaining % 60]
