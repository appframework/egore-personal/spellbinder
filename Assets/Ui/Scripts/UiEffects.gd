extends Control

var health
var effectMap

func _ready():
	health = get_node("/root/Level/Avatar/Health")
	effectMap = Dictionary() # Effect -> Image

func _process(_delta):
	var i = 0
	var usedEffects = Dictionary()
	for effect in health.effects:
		# If the effect does not have a UI effect, skip it
		if !effect.effect.effectUiImage:
			continue
		usedEffects.Add(effect.effect)
		var image
		if !effectMap.ContainsKey(effect.effect):
			pass
			#image = Instantiate(imagePrefab, transform, false)
			#image.name = effect.effect.name + " applied"
			#effectMap[effect.effect] = image
			#image.sprite = effect.effect.effectUiImage
		else:
			image = effectMap[effect.effect]
		var v2 = image.transform.anchoredPosition as Vector3
		v2.y = -24 - (i * 50)
		i += 1
		image.transform.anchoredPosition = v2

	var toRemove = []
	for key in effectMap:
		if !usedEffects.Contains(key):
			toRemove.Add(key)

	for key in toRemove:
		var image = effectMap[key]
		image.free()
		effectMap.Remove(key)
