extends Node
class_name TeamPlayers

var team: Team
@onready var teamname: Label = $Teamname
@onready var playerLevels: Label = $"Player Levels"
@onready var playerNames: Label = $"Player names"

func _ready():
	playerLevels.text = ""
	playerNames.text = ""

func init(newTeam: Team) -> void:
	team = newTeam
	teamname.text = str(team.name)
	teamname.set("custom_colors/font_color", team.color)
	playerLevels.set("custom_colors/font_color", team.color)
	playerNames.set("custom_colors/font_color", team.color)
	team.connect("player_joined", handle_player_joined)

func handle_player_joined(team_) -> void:
	# Render the players list
	playerLevels.text = ""
	playerNames.text = ""
	if team_:
		for player in team_.players:
			playerLevels.text += "%d%s\n" % [player.level, player.profession.shortName]
			playerNames.text += player.playername + "\n"
