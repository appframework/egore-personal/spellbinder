extends RigidBody3D
class_name BouncingProjectile

@export var speed: float = 50.0
@export var damage: int = 20
@export var lifetime: float = 5.0

func shoot():
	print("Firing bouncing projectile")
	apply_impulse(-transform.basis.z * speed, transform.basis.z)

func _physics_process(delta):
	lifetime -= delta
	if lifetime < 0:
		print("Bouncing projectile reached end of lifetime")
		sleeping = true
		queue_free()

func _on_Area_area_entered(area):
	print("Bouncing projectile hit entity")
	if area.get_parent().is_in_group("Player"):
		area.get_parent().health.doDamage(damage)
		# TODO add desolving effect
		# Destroy only when hitting a player
		sleeping = true
		queue_free()
